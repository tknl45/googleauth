'use strict'

//驗證
const { google } = require('googleapis')

//檔案讀取參考 https://www.ithome.com.tw/guest-post/98590 實作Web Application認證
const key = require('./auth.json')

//查詢scope https://developers.google.com/identity/protocols/googlescopes
const scopes = 'https://www.googleapis.com/auth/cloud-platform';

//呼叫使用jwt
const jwt = new google.auth.JWT(
  key.client_email, 
  null, 
  key.private_key, 
  scopes);



//取得 google access_token
jwt.authorize((err, response) => {
  console.log(err, response);
  google.dialogflow("v2").projects.getAgent(
    {auth:jwt,parent:"api-project-410849963392"},
  (err, result) => {
    console.log(err, result)
  });

});



